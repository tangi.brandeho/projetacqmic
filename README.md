# ProjetAcqMic

## Présentation Générale

### Plateforme cible

![](Assets/Images/stm32_valid.jpg)
La carte STM32F429I-DISC1 est une plateforme de développement pour systèmes embarqués basée sur le microcontrôleur STM32F429ZIT6 avec une architecture ARM Cortex-M4 cadencée à 180 MHz. Elle offre une gamme de fonctionnalités intégrées telles qu'un écran LCD TFT couleur de 2,4 pouces, un joystick, des boutons-poussoirs et des connecteurs d'extension. Grâce à ses interfaces USB, Ethernet et sans fil, elle est adaptée à diverses applications, notamment l'Internet des objets (IoT) et les systèmes de contrôle industriels. Sa conception robuste et ses capacités de traitement en font un choix populaire pour les projets nécessitant des performances élevées dans le domaine des systèmes embarqués

Sur la carte STM32F429I-DISC1, plusieurs protocoles et interfaces peuvent être utilisés pour la communication et le contrôle des périphériques. Voici une description de quelques-uns des principaux protocoles disponibles :

DAM (DMA - Direct Memory Access) : Le contrôleur DMA permet un transfert de données direct entre la mémoire et les périphériques sans intervention du processeur principal. Cela permet d'améliorer les performances en libérant le processeur pour d'autres tâches pendant les transferts de données.

DAC (Digital-to-Analog Converter) : Le convertisseur numérique-analogique permet de convertir des signaux numériques en signaux analogiques. Sur la carte STM32F429I-DISC1, le DAC peut être utilisé pour générer des signaux analogiques, utiles par exemple dans des applications audio ou de contrôle de capteurs.

UART (Universal Asynchronous Receiver-Transmitter) : Les interfaces UART permettent la communication série asynchrone entre le microcontrôleur et d'autres périphériques. Elles sont largement utilisées pour la communication avec des modules GPS, des capteurs, des dispositifs sans fil, etc.

SPI (Serial Peripheral Interface) : L'interface SPI est un protocole série synchrone utilisé pour la communication entre plusieurs périphériques sur de courtes distances. Elle est souvent utilisée pour interconnecter des capteurs, des écrans LCD, des modules de mémoire, etc.

I2C (Inter-Integrated Circuit) : L'interface I2C est un bus de communication série bidirectionnel utilisé pour la communication entre plusieurs composants sur une carte électronique. Elle est couramment utilisée pour interconnecter des capteurs, des dispositifs d'entrée/sortie, des mémoires EEPROM, etc.

CAN (Controller Area Network) : Le bus CAN est un protocole de communication série utilisé principalement dans les applications automobiles et industrielles pour le contrôle et la surveillance de multiples nœuds interconnectés.

![](Assets/Images/archi_generale_1.png)

## Objectifs du projet

Ce projet vise à explorer la plateforme STM32 ainsi que les outils associés en mettant en œuvre une chaîne de traitement du signal audio. Nous cherchons à intégrer un micro MEMs en utilisant le protocole SAI (Serial Audio Interface) tout en exploitant les fonctionnalités avancées du DMA (Direct Access Memory), du PDM (Pulse Density Modulation), du PCM (Pulse Coding Modulation) et du DAC (Digital-to-Analog Converter).

## Logiciel de développement

STMCubeIDE est un environnement de développement intégré (IDE) développé par STMicroelectronics pour la programmation des microcontrôleurs de la famille STM32. Il propose une interface utilisateur intuitive et intègre étroitement STM32CubeMX, un outil de configuration graphique permettant de générer du code initial pour les projets STM32. STMCubeIDE prend en charge l'ensemble de la gamme de microcontrôleurs STM32, offrant ainsi un support complet du matériel. Il offre des fonctionnalités de débogage avancées, notamment le support des débogueurs matériel ST-Link. 


## Explication du fonctionnement Général

### DMA et buffer ping-pong explication

DMA est une méthode de communication permettant aux périphériques d'adresser directement de l'information vers la mémoire en passant par un contrôleur matériel dit DMA en n'utilisant le CPU que pour le début de la communication et sa terminaison.

L'intérêt principal est d'alléger le travail du processeur dans le cadre d'applications multi-tâches mettant en oeuvre des périphériques.

Différentes étapes ont lieu : 
- le périphérique envoi une requête au DMA pour transférer des données, le DMA accepte et demande au processeur d'attendre quelques cycles d'horloge (hold).
- Le processeur cède l'accès au bus de données et donne le maintien au DMA
- Le DMA valide alors la demande du périphérique et lui donne accès au bus de données pour envoyer ou récupérer de l'information depuis la mémoire.
- Lorsque le transfert est terminé, le DMA déclenche une interruption pour indiquer au processeur que la tâche est terminée et qu'il peut récupérer l'accès au bus système.

Afin de maximiser l'accès à la zone de mémoire on peut faire appel à la notion de buffer dit ping pong. L'idée est de diviser en deux la mémoire de ne lire que la moitié de la mémoire pendant que l'autre moitié est en cours d'écriture. En effet dans le cadre d’une acquisition en continu cela permet d’écrire et de lire puis de traiter simultanément les données en mémoire.
On exploite pour cela les interruptions. Lorsque le buffer est rempli à moitié (24 éléments de 64 bits ont été écrits) une interruption survient et on bascule dans la position B où le buffer qui a été précédemment rempli est lu par le bloc de traitement PDM2PCM puis vidé pendant que l’autre buffer est rempli. Cela impose que les buffers soient lus et écrits à la même cadence sous peine de perdre de l’information.

Voici les fonctions principales du code :

```
#define BUFFER_SIZE 1024
uint8_t pdm_data[BUFFER_SIZE];
uint32_t Half_Size = BUFFER_SIZE/2; //512 pour interruption
uint16_t pcm_data[48000];
int dmaLeft = 0;
int dmaRight = 0;
int last_pcm_index = 0;
void HAL_SAI_RxHalfCpltCallback(SAI_HandleTypeDef *hsai){
  dmaLeft = 1; // Allow first half buffer processing
}
void HAL_SAI_RxCpltCallback(SAI_HandleTypeDef *hsai){
  dmaRight = 0; // Allow second half buffer processing
}	
```
Ces deux interruptions permettent ainsi de gérer à l'aide de 'flag', la bonne moitié du buffer. Le code permettant de convertir les données pdm en pcm est ci-dessous:

```
void pdm_to_pcm(uint8_t* pdm_data, uint16_t* pcm_data, uint32_t Half_Size) {
  uint64_t* ptr = pdm_data; //pointeur de 64bits

  for (ptr; ptr < Half_Size/8; ptr++) {
    uint32_t pdm_bit_count = __builtin_popcountll(*ptr);
    pcm_data[ptr+last_pcm_index] = pdm_bit_count << 6;
    last_pcm_index = Half_Size;
  }
}

```
La boucle while du main :

```
while (1)
  {
	  /* USER CODE BEGIN WHILE */
	  if (last_pcm_index == BUFFER_SIZE){
		  HAL_SAI_DMAStop(*hsai_BlockA1);  //STOP le DMA
	  }
	  else {
		  if (dmaLeft){
			  pdm_to_pcm(pdm_data, pcm_data, Half_Size);
			  dmaLeft = 0;
		  	  }
		  else if (dmaRight){
			  pdm_to_pcm((uint8_t*)(pdm_data+Half_Size), pcm_data, Half_Size);
			  dmaRight = 0;
		  	  }
	  }
	  /* USER CODE END WHILE */
  }
```

### Filtrage PDM2PCM
Ici l’objectif premier est de recréer le signal discret semblable au signal audio reçu en entrée, qui permettra par la suite au DAC de construire un signal audio audible en sortie de la carte. En entrée de cette conversion on a un signal de type PDM généré par le micro qui est composé d’informations binaires uniquement codées sur 64 bits.

![](Assets/Images/PDM2PCM.png)

Cette conversion est réalisée en exploitant la fonction __builtin_popcountll() qui compte le nombre de bits définis à 1 dans un entier signé. Afin d’obtenir la valeur il faut diviser le résultat du calcul de la fonction par le nombre de bits d’un échantillon soit 8 bits. Pour mieux comprendre, on cherche ici à mesurer à quel pourcentage de la pleine valeur (64 bits) du micro on se trouve, ainsi si _builtin_popcountll(échantillon)/8 = 16 bits, alors on est au quart de la pleine valeur du micro et c’est donc également ce qui doit être émis.

En sortie de cette conversion, il y a souvent un bruit en haute fréquence qui apparaît, il est donc nécessaire d’implémenter un filtre passe-bas de type FIR. Ce fut l’objet de deux séances de conception de filtre d’abord sous Matlab puis en C afin de pouvoir implémenter ce filtre dans la chaîne développée pour ce projet.
 Les paramètres de filtrage ont été définis comme suit :

```
Fs = 48000;  % Sampling Frequency
Fpass = 3500;            % Passband Frequency
Fstop = 4500;            % Stopband Frequency
Dpass = 0.057501127785;  % Passband Ripple
Dstop = 0.0001;          % Stopband Attenuation
dens  = 20;              % Density Factor
```
Ce qui nous semblait être la meilleure façon de réduire les interférences en hautes fréquences générées par la conversion PDM2PCM tout en ne dégradant que très peu la qualité de son en sortie.
A la fin du BE nous étions en mesure de simuler le comportement de la carte dans le cadre des calculs récursifs nécessités par le filtre en les réalisant sans virgule fixe. La formule proposée dans la présentation a été retenue :
```
S=PartieEntiereSup(log2(Somme(abs(bi))))
𝑁𝑓 < 𝑁 − 𝑆 − 𝑙𝑜𝑔2(|max(𝑥 𝑛 | + 1)
```
En fin de simulation du filtrage sous Matlab on obtient la figure suivante montrant en superposition le signal audio d'entrée et Le signal PDM après le filtrage qui sera ensuite injecté dans le bloc DMA.

![](Assets/Images/Figure_1.png)

Deux fichiers C ont été générés après la simulation Matlab : un fichier contenant les coefficients du filtre et un second fichier contenant directement les instructions pour les traitements réalisés par le filtre et générant le signal de sortie.

```
#ifndef COEFFS_FIR_H
#define COEFFS_FIR_H

#define N_FRAC 23
#define N_COEFFS 123

const int32_t coeffs_fir[N_COEFFS]={1145, 1616, 2161, 2094, 837, -2202, -7466, -15060, -24597, -35110, -45098, -52710, -56083, -53754, -45085, -30584, -12013, 7773, 25267, 37003, 40326, 34112, 19218, -1475, -23483, -41598, -51041, -48652, -33840, -9007, 20707, 48385, 66848, 70409, 56463, 26500, -13787, -55420, -87981, -102040, -91631, -56174, -1317, 61641, 117775, 151695, 151232, 110846, 33952, -66490, -169438, -249165, -280043, -241814, -124288, 69566, 322864, 606703, 884459, 1117772, 1273093, 1327567, 1273093, 1117772, 884459, 606703, 322864, 69566, -124288, -241814, -280043, -249165, -169438, -66490, 33952, 110846, 151232, 151695, 117775, 61641, -1317, -56174, -91631, -102040, -87981, -55420, -13787, 26500, 56463, 70409, 66848, 48385, 20707, -9007, -33840, -48652, -51041, -41598, -23483, -1475, 19218, 34112, 40326, 37003, 25267, 7773, -12013, -30584, -45085, -53754, -56083, -52710, -45098, -35110, -24597, -15060, -7466, -2202, 837, 2094, 2161, 1616, 1145};

#endif
```

## Réalisation pratique de la chaîne

Nous allons maintenant définir les différents réglages de chaque bloc de la chaîne à rentrer au niveau du .ioc.

### Réglage du SAI

Pour la configuration du SAI il faut se rendre dans le .ioc puis `multimedia->SAI1`.

Les réglages à appliquer sont les suivants :

Tout d'abord il faut se rendre dans la fenêtre `DMA Settings` : 
![](Assets/Images/SAI_DMA_settings.png)

Puis dans la fenêtre `NVIC_Configuration` :

![](Assets/Images/SAI_config.png)

Dans la fenêtre `Parameters Settings` :

![](Assets/Images/SAI_parameters_settings.png)

![](Assets/Images/SAI_parameters_settings_suite.png)

Enfin, dans la fenêtre `GPIO settings`:

![](Assets/Images/DAC_GPIO.png)

### Réglage du DAC

Tout d'abord il faut se rendre dans la fenêtre `DMA settings`.

![](Assets/Images/DAC_DMA_config.png)

Puis dans la fenêtre `GPIO settings`

![](Assets/Images/DAC_modeandconfig.png)

### Réglage du Timer4 et des clocks configuration

Enfin il faut régler le Timer4 de la façon suivante dans l'onglet `Parameter Settings` : 

![](Assets/Images/TIM4_Modeandconfig.png)

Dans l'onglet `Clock Configuration` il faut avoir la configuration suivante : 

![](Assets/Images/clocks_config.png)






